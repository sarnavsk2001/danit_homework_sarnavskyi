import { useEffect } from "react"
import { Routes, Route } from "react-router-dom"
import "./App.scss"
import CardsList from "./components/CardsList/CardsList"
import Header from "./components/Header/Header"
import Modal from "./components/Modal/Modal"
import Cart from "./pages/Cart/Cart"
import Favourite from "./pages/Favourite/Favourite"
import Error from "./pages/Error/Error"
import { useDispatch, useSelector } from "react-redux"
import { fetchProducts } from "./store/products/actions"
import BuyModal from "./components/BuyModal/BuyModal"
import { useState } from "react"
import { ViewContext } from "./ViewContext"

const App = () => {
  const dispatch = useDispatch()

  const modalIsOpen = useSelector((state) => state.cart.modalIsOpen)
  const buyModalIsOpen = useSelector((state) => state.cart.buyModalIsOpen)
  const cartList = useSelector((state) => state.cart.cartList)

  const [isTableCartView, setIsTableView] = useState(false)

  useEffect(() => {
    dispatch(fetchProducts())
  }, [dispatch])

  return (
    <div className={"body-container"}>
      <Header />
      <div className="modal-container">{modalIsOpen && <Modal />}</div>

      <Routes>
        <Route
          path="/"
          element={
            <div className="app-container">
              <div className="app-container__toggle-box btn-box">
                <button
                  onClick={() => {
                    setIsTableView(false)
                  }}
                  className={isTableCartView ? "btn-box" : "orange btn-box"}
                >
                  Cards
                </button>
                <button
                  onClick={() => {
                    setIsTableView(true)
                  }}
                  className={!isTableCartView ? "btn-box" : "orange btn-box"}
                >
                  Table
                </button>
              </div>
              <div className="app-container__cardlist-box">
                <ViewContext.Provider value={isTableCartView}>
                  <CardsList />
                </ViewContext.Provider>
              </div>
            </div>
          }
        />
        <Route
          path="/cart"
          element={
            <div className="cart-container">
              <div className="cart-container__buy-modal">
                {cartList.length > 0 && buyModalIsOpen && <BuyModal />}
              </div>
              <Cart />
            </div>
          }
        />
        <Route
          path="/favourite"
          element={
            <div className="favourite-container">
              <Favourite />
            </div>
          }
        />
        <Route path="*" element={<Error />} />
      </Routes>
    </div>
  )
}

export default App

App.defaultProps = {
  modalIsOpen: false,
}
