const reducer = (state = [], action) => {
  switch (action.type) {
    case "ADD_FAVOURITE": {
      return [...state, action.payload];
    }
    case "REMOVE_FAVOURITE": {
      return state.filter((card) => card.vendorCode !== action.payload);
    }
    default: {
      return state;
    }
  }
};

export default reducer;
