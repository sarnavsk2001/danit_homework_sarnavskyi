import reducer from "./reducer";

describe("favourites reducer", () => {
  const state = [
    {
      name: "asus",
      title: "laptop 1",
      vendorCode: 111111,
    },
    {
      name: "acer",
      title: "laptop 2",
      vendorCode: 222222,
    },
  ];

  it("should return default value", () => {
    expect(reducer(undefined, {})).toEqual([]);
  });

  describe("for ADD_FAVOURITE action", () => {
    it("should add favourite", () => {
      expect(
        reducer(state, {
          type: "ADD_FAVOURITE",
          payload: { name: "apple", title: "laptop 3", vendorCode: 333333 },
        })
      ).toEqual([
        {
          name: "asus",
          title: "laptop 1",
          vendorCode: 111111,
        },
        {
          name: "acer",
          title: "laptop 2",
          vendorCode: 222222,
        },
        {
          name: "apple",
          title: "laptop 3",
          vendorCode: 333333,
        },
      ]);
    });
  });

  describe("for REMOVE_FAVOURITE", () => {
    it("should remove favourite", () => {
      expect(
        reducer(state, {
          type: "REMOVE_FAVOURITE",
          payload: 222222,
        })
      ).toEqual([
        {
          name: "asus",
          title: "laptop 1",
          vendorCode: 111111,
        },
      ]);
    });
  });
});
