import PropTypes from "prop-types"
import { useSelector } from "react-redux"
import { Link } from "react-router-dom"

const Header = () => {
  const favouriteList = useSelector((state) => state.favouriteList)
  const cartList = useSelector((state) => state.cart.cartList)

  return (
    <div className="header">
      <Link to="/" className="header__name">
        <h3>VladetecStore</h3>
      </Link>
      <Link to="/cart" className="header__basket basket-list">
        <img className="basket-list__icon" src={"./shopping-cart.svg"} />
        <span className="basket-list__number">{cartList.length}</span>
      </Link>
      <Link to="/favourite" className="header__favourite favourite-list">
        <img className="favourite-list__icon" src={"./favourite.svg"} />
        <span className="favourite-list__number">{favouriteList.length}</span>
      </Link>
    </div>
  )
}

export default Header
