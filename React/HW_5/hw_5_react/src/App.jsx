import { useEffect } from "react";
import { Routes, Route } from "react-router-dom";
import "./App.scss";
import CardsList from "./components/CardsList/CardsList";
import Header from "./components/Header/Header";
import Modal from "./components/Modal/Modal";
import Cart from "./pages/Cart/Cart";
import Favourite from "./pages/Favourite/Favourite";
import Error from "./pages/Error/Error";
import { useDispatch, useSelector } from "react-redux";
import { fetchProducts } from "./store/products/actions";
import BuyModal from "./components/BuyModal/BuyModal";

const App = () => {
  const dispatch = useDispatch();
  const modalIsOpen = useSelector((state) => state.cart.modalIsOpen);
  const buyModalIsOpen = useSelector((state) => state.cart.buyModalIsOpen);
  const cartList = useSelector((state) => state.cart.cartList);
  useEffect(() => {
    dispatch(fetchProducts());
  }, []);

  return (
    <div className={"body-container"}>
      <Header />
      <div className="modal-container">{modalIsOpen && <Modal />}</div>

      <Routes>
        <Route
          path="/"
          element={
            <div className="app-container">
              <CardsList />
            </div>
          }
        />
        <Route
          path="/cart"
          element={
            <div className="cart-container">
              <div className="cart-container__buy-modal">
                {cartList.length > 0 && buyModalIsOpen && <BuyModal />}
              </div>
              <Cart />
            </div>
          }
        />
        <Route
          path="/favourite"
          element={
            <div className="favourite-container">
              <Favourite />
            </div>
          }
        />
        <Route path="*" element={<Error />} />
      </Routes>
    </div>
  );
};

export default App;

App.defaultProps = {
  modalIsOpen: false,
};
