import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import { addInCart } from "../../store/cart/actions";
import { addFavorite } from "../../store/favourite/actions";

const Card = ({ name, price, color, imagePath, my_key, myKeyButton }) => {
  const dispatch = useDispatch();
  const productList = useSelector((state) => state.product.productList);
  const favouriteList = useSelector((state) => state.favouriteList);
  const cartList = useSelector((state) => state.cart.cartList);

  const clickFavourite = (event, key) => {
    const favouriteCardThereIs = favouriteList.some(
      (product) => product.vendorCode === key
    );

    if (!favouriteCardThereIs) {
      let favouriteCard = productList.filter(
        (product) => product.vendorCode === key
      );
      dispatch(addFavorite(favouriteCard[0]));
    }
  };

  const buttonBuyClick = (event, key) => {
    const buyCardThereIs = cartList.some(
      (product) => product.vendorCode === key
    );
    if (!buyCardThereIs) {
      let productCart = productList.filter(
        (product) => product.vendorCode === key
      );
      dispatch(addInCart(productCart[0]));
      //   setModalIsOpen(true);
    }
  };

  return (
    <div className="card">
      <img
        className="card__img"
        src={imagePath}
        alt="#"
        max-width="100%"
        height="110"
      ></img>
      <span className="card__title">{name}</span>
      <span
        onClick={(event) => clickFavourite(event, my_key)}
        className="card__favoutite"
        id={my_key}
      >
        ♡
      </span>
      <p className="card__description">
        Lorem ipsum dolor sit amet, consect adipiscing elit dalut.
      </p>
      <p className="card__color">Color: {color}</p>
      <span className="card__price">Price: {price}$</span>
      <button
        className="card__btn"
        onClick={(event) => buttonBuyClick(event, myKeyButton)}
      >
        Add to cart
      </button>
    </div>
  );
};

export default Card;

Card.propTypes = {
  name: PropTypes.string,
  price: PropTypes.number,
  color: PropTypes.string,
  imagePath: PropTypes.string,
  my_key: PropTypes.number,
  myKeyButton: PropTypes.number,
};

Card.defaultProps = {
  color: "black",
};
