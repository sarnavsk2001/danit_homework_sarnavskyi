import Card from "../Card/Card";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";

const CardsList = () => {
  const productList = useSelector((state) => state.product.productList);
  const isLoading = useSelector((state) => state.product.isLoading);
  const hasError = useSelector((state) => state.product.hasError);

  return (
    <div className="cardlist-container">
      {hasError ? (
        <span>Opps, error! Please, try again!</span>
      ) : isLoading ? (
        <span className="cardlist-container__loading">Loading...</span>
      ) : (
        productList.map((card) => {
          return (
            <Card
              key={card.vendorCode}
              my_key={card.vendorCode}
              myKeyButton={card.vendorCode}
              name={card.name}
              price={card.price}
              color={card.color}
              imagePath={card.imagePath}
            />
          );
        })
      )}
    </div>
  );
};

export default CardsList;

CardsList.propTypes = {
  productList: PropTypes.array,
};
