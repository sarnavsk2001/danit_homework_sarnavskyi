import { Component } from "react";
import "./App.scss";
import getProductList from "./api/getProductList";
import CardsList from "./components/CardsList/CardsList";
import Header from "./components/Header/Header";
import Modal from "./components/Modal/Modal";

class App extends Component {
  constructor() {
    super();
    this.state = {
      productList: [],
      favouriteList: [],
      cartList: [],
      modalIsOpen: false,
    };
  }

  componentDidMount() {
    getProductList().then((data) => {
      this.setState({ productList: data });
    });

    // let localStorageFavourites = localStorage.getItem("favouriteList");
    // let localStorageCart = localStorage.getItem("cartList");
    // this.setState({
    //   cartList: [...this.state.cartList, localStorageCart],
    //   favouriteList: [...this.state.favouriteList, localStorageFavourites],
    // });
  }

  buttonClick = (event, key) => {
    let productCart = this.state.productList.filter(
      (product) => product.vendorCode === key
    );

    this.setState({
      cartList: [...this.state.cartList, productCart[0]],
      modalIsOpen: true,
    });
    console.log(this.state.cartList);
    localStorage.setItem("cartList", JSON.stringify(this.state.cartList));
  };

  bodyContainerClick = (event) => {
    this.setState({ modalIsOpen: false });
  };

  clickFavourite = (event, key) => {
    const favouriteCardThereIs = this.state.favouriteList.some(
      (product) => product.vendorCode === key
    );

    if (!favouriteCardThereIs) {
      let favouriteCard = this.state.productList.filter(
        (product) => product.vendorCode === key
      );

      this.setState((state) => {
        return { favouriteList: [...state.favouriteList, favouriteCard[0]] };
      });
      console.log(this.state.favouriteList);

      event.currentTarget.innerHTML = "❤️";
      localStorage.setItem(
        "favouriteList",
        JSON.stringify(this.state.favouriteList)
      );
    }
  };

  render() {
    return (
      <div
        className={
          this.state.modalIsOpen
            ? "body-container grey-background"
            : "body-container"
        }
      >
        <Header
          productsCart={this.state.cartList.length}
          productsFavourite={this.state.favouriteList.length}
        />
        <div className="modal-container">
          {this.state.modalIsOpen && (
            <Modal updateData={this.bodyContainerClick} />
          )}
        </div>
        <div className="app-container">
          <CardsList
            productList={this.state.productList}
            clickOpenModal={this.buttonClick}
            clickFavourite={this.clickFavourite}
          />
        </div>
      </div>
    );
  }
}

export default App;

App.defaultProps = {
  modalIsOpen: false,
};
