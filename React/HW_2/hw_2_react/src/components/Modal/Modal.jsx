import { Component } from "react";
import "./Modal.jsx";
import ReactDOM from "react-dom";
import PropTypes from 'prop-types';

class Modal extends Component {
  render() {
    const { updateData } = this.props;
    return (
      <div className="modal">
        <div className="modal__header header">
          <button onClick={updateData} className="header__button">X</button>
        </div>
        <div className="modal__main main">
          <h3 className="main__text">
          Your products have been added to the cart
          </h3>
          <div>
            <button onClick={updateData} className="main__button">Ok</button>
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;

Modal.propTypes = {
  updateData: PropTypes.func
}

