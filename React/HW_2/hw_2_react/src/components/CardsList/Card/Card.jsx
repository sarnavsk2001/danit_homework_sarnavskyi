import { Component } from "react";
import PropTypes from 'prop-types';


class Card extends Component {
  constructor() {
    super();
  }

  render() {
    const { name, price, color, imagePath, my_key,  myKeyButton, clickOpenModal, clickFavourite } = this.props;
    return (
      <div className="card">
        <img
          className="card__img"
          src={imagePath}
          alt="#"
          max-width="100%"
          height="110"
        ></img>
        <span className="card__title">{name}</span><span onClick={event=> clickFavourite(event, my_key)} className="card__favoutite" id={my_key}>♡</span>
        <p className="card__description">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit
        </p>
        <p className="card__color">Color: {color}</p>
        <span className="card__price">{price}$</span>
        <button className="card__btn" onClick={event=> clickOpenModal(event, myKeyButton)}>
          Add to cart
        </button>
      </div>
    );
  }
}

export default Card;


Card.propTypes = {
  name: PropTypes.string,
  price: PropTypes.number,
  color: PropTypes.string,
  imagePath: PropTypes.string,
  my_key: PropTypes.number,
  myKeyButton: PropTypes.number,
  clickOpenModal: PropTypes.func,
  clickFavourite: PropTypes.func
}

Card.defaultProps= {
 color: 'black'
}