import { Component } from "react";
import Card from "./Card";
import PropTypes from 'prop-types';

class CardsList extends Component {
  constructor() {
    super();
  }

  render() {
    return (
      <div className="cardlist-container">
        {this.props.productList.map((card) => {
          return (
            <Card
              key={card.vendorCode}
              my_key={card.vendorCode}
              myKeyButton={card.vendorCode}
              name={card.name}
              price={card.price}
              color={card.color}
              imagePath={card.imagePath}
              clickOpenModal={this.props.clickOpenModal}
              clickFavourite={this.props.clickFavourite}
            />
          );
        })}
      </div>
    );
  }
}

export default CardsList;

CardsList.propTypes = {
  productList: PropTypes.array
}
