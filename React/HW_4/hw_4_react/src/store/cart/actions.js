export const addInCart = (productInCart) => {
  return {
    type: "ADD_PRODUCT_IN_CART",
    payload: productInCart,
  };
};

export const removeInCart = (key) => {
  return {
    type: "REMOVE_PRODUCT_FROM_CART",
    payload: key,
  };
};

export const hideModalCart = (value) => {
  return {
    type: "HIDE_MODAL",
    payload: value,
  };
};
