const Error = () => {
  return (
    <div className="error">
      <h2 className="error__message">
        Error! Please try again with right URL!
      </h2>
    </div>
  );
};

export default Error;
