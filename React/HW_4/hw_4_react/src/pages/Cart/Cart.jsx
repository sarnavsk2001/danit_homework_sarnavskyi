import { useSelector } from "react-redux";
import CartCard from "../../components/CartCard/CartCard";

const Cart = () => {
  const cartList = useSelector((state) => state.cart.cartList);

  const amountSubstractingCart = () => {
    let totalSum = 0;
    cartList.map((card) => {
      totalSum += card.price;
      return totalSum;
    });
    return totalSum;
  };

  return (
    <div className="cart-container">
      {cartList.map((card) => {
        return (
          <CartCard
            key={card.vendorCode}
            myKeyButton={card.vendorCode}
            name={card.name}
            price={card.price}
            color={card.color}
            imagePath={card.imagePath}
          />
        );
      })}

      {cartList.length > 0 ? (
        <p className="cart-container__total-sum">
          Total: {amountSubstractingCart()}$
        </p>
      ) : (
        <p className="cart-container__message">No items in cart</p>
      )}
    </div>
  );
};

export default Cart;
