import "./App.scss";
import { Component } from "react";
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";
import modalWindowDeclarations from "./api/index";

class App extends Component {
  constructor() {
    super();
    this.state = {
      modalIsOpen: false,
      modalInformation: null,
    };
  }

  buttonClick = (event) => {
    const modalID = event.target.id;
    this.setState({
      modalIsOpen: true,
      modalInformation: modalWindowDeclarations.find(
        (item) => item.id == modalID
      ),
    });
  };

  updateData = (value) => {
    this.setState({ modalIsOpen: value });
  };

  bodyContainerClick = (event) => {
      this.setState({ modalIsOpen: false });
  };

  render() {
    return (
      <div onMouseUp={this.bodyContainerClick} className={this.state.modalIsOpen ? "body-container grey-background" : "body-container"}>
        <div className="button-container">
          {!this.state.modalIsOpen && (
            <Button
              backgroundColor="yellow"
              text="Open first modal"
              onClick={this.buttonClick}
              modalOpenNumber={1}
            />
          )}
          {!this.state.modalIsOpen && (
            <Button
              backgroundColor="blue"
              text="Open second modal"
              onClick={this.buttonClick}
              modalOpenNumber={2}
            />
          )}
        </div>

        <div>
          <div className="modal-container">
            {this.state.modalIsOpen && (
              <Modal
                header={this.state.modalInformation.title}
                closeButton={this.state.modalInformation.closeButton}
                text={this.state.modalInformation.description}
                actions={this.state.modalInformation.actions}
                updateData={this.updateData}
              />
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
