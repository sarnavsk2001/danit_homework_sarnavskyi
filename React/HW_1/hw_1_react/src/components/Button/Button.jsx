import { Component } from "react";
import './Button.jsx';

export default class Button extends Component {

  render() {
    const { backgroundColor, text, onClick, modalOpenNumber } = this.props;
    return (
      <>
        <button className="open-modal-btn" style={{background: backgroundColor}} id={modalOpenNumber} onClick={onClick}>
          {text}
        </button>
      </>
    );
  }
}
