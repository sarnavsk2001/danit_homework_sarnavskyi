const modalWindowDeclarations = [
    {
      id: 1,
      closeButton: true,
      title: "Do you want delete this file?",
      actions: (
        <>
          <button className="main__button">Ok</button>
          <button className="main__button">Cancel</button>
        </>
      ),
      description:
        "Once you delete this file, it won`t be possible to undo this action. Are you sure you want to delete?",
    },
    {
      id: 2,
      closeButton: false,
      title: "title for modal 2",
      actions: (
        <>
          <button className="main__button">Ok</button>
        </>
      ),
      description: "description for modal 2",
    },
  ];
  
export default modalWindowDeclarations;