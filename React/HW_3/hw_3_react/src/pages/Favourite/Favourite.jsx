import Card from "../../components/Card/Card";

const Favourite = ({ favouriteList, clickBuyFavourite, clickFavourite }) => {
  return (
    <div>
      <div className="cardlist-container">
        {favouriteList.map((card) => {
          return (
            <Card
              key={card.vendorCode}
              my_key={card.vendorCode}
              myKeyButton={card.vendorCode}
              name={card.name}
              price={card.price}
              color={card.color}
              imagePath={card.imagePath}
              clickOpenModal={clickBuyFavourite}
              clickFavourite={clickFavourite}
            />
          );
        })}
      </div>
      <div>
        {!favouriteList.length > 0 && (
          <p className="favourite-container__message">No items in favourite</p>
        )}
      </div>
    </div>
  );
};

export default Favourite;
