const CartCard = ({
  name,
  price,
  color,
  imagePath,
  myKeyButton,
  removeClickFromCart,
}) => {
  return (
    <div className="cart-card">
      <img
        className="cart-card__img"
        src={imagePath}
        alt="#"
        max-width="100%"
        height="110"
      ></img>
      <div  className="cart-card__text-container cart-text">
        <span className="cart-text__title">{name}</span>
        <p className="cart-text__description">
          Lorem ipsum dolor sit amet, consect adipiscing elit dalut.
        </p>
        <p className="cart-text__color">Color: {color}</p>
        <span className="cart-text__price">Price: {price}$</span>
      </div>
      <button
        className="cart-card__btn"
        onClick={(event) => removeClickFromCart(event, myKeyButton)}
      >
        X
      </button>
    </div>
  );
};

export default CartCard;
