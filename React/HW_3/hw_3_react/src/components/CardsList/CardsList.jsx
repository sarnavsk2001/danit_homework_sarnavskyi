import Card from "../Card/Card";
import PropTypes from "prop-types";

const CardsList = ({ productList, clickOpenModal, clickFavourite }) => {
  return (
    <div className="cardlist-container">
      {productList.map((card) => {
        return (
          <Card
            key={card.vendorCode}
            my_key={card.vendorCode}
            myKeyButton={card.vendorCode}
            name={card.name}
            price={card.price}
            color={card.color}
            imagePath={card.imagePath}
            clickOpenModal={clickOpenModal}
            clickFavourite={clickFavourite}
          />
        );
      })}
    </div>
  );
};

export default CardsList;

CardsList.propTypes = {
  productList: PropTypes.array,
};
