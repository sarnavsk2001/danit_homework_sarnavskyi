import PropTypes from "prop-types";
import { Link } from "react-router-dom";

const Header = ({ productsCart, productsFavourite }) => {
  return (
    <div className="header">
      <Link to="/" className="header__name"><h3>VladetecStore</h3></Link>
      <Link to="/cart" className="header__basket basket-list">
        <img className="basket-list__icon" src={"./shopping-cart.svg"} />
        <span className="basket-list__number">{productsCart}</span>
      </Link>
      <Link to="/favourite" className="header__favourite favourite-list">
        <img className="favourite-list__icon" src={"./favourite.svg"} />
        <span className="favourite-list__number">{productsFavourite}</span>
      </Link>
    </div>
  );
  
};

Header.propTypes = {
  productsCart: PropTypes.number,
  productsFavourite: PropTypes.number,
};

export default Header;
