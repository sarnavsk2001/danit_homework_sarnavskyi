window.onload = () => {
    if (localStorage.getItem('theme') === 'dark-theme') {
        document.getElementById("theme-link").setAttribute("href", "./css/dark.css");
    } else {
        document.getElementById("theme-link").setAttribute("href", "./css/light.css");
    }
}

document.querySelector(".theme-btn").addEventListener("click", () => {
    let lightTheme = "./css/light.css";
    let darkTheme = "./css/dark.css";

    let nowTheme = document.getElementById("theme-link").getAttribute("href");
    localStorage.clear();

    if (nowTheme === lightTheme) {
        nowTheme = darkTheme;
        localStorage.setItem('theme', 'dark-theme');

    } else {
        nowTheme = lightTheme;
        localStorage.setItem('theme', 'light-theme');
    }

    document.getElementById("theme-link").setAttribute("href", nowTheme);
});

