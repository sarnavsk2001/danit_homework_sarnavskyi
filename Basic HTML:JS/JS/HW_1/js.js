//1)Обьяснить своими словами разницу между обьявлением переменных через var, let и const.
/*
Обьявления через var уже не юзают в наше время (так как он используется только в легаси кодах).
Хорошим тоном считается обьявление через const, но нужно смотреть по ситуации, так как const - константа
и дальше в коде не сможет менятся (в отличии от let).
*/


//2)Почему объявлять переменную через var считается плохим тоном?
/*Обьявления через var уже не юзают в наше время (так как он используется только в легаси кодах).
Область видимости var будет ограничеваться. Так же объявление через var обрабатываются в начале выполнения функции.*/



let userAnswerName = prompt('Ваше имя?');
let userAnswerAge = Number(prompt('Ваш возраст?'));

if (userAnswerAge < 18) {
    alert("You are not allowed to visit this website");
} else if (userAnswerAge >= 18 && userAnswerAge <= 22) {
    let userAnswerContinue = confirm("Are you sure you want to continue");
    if (userAnswerContinue)
    {
        alert("Welcome");
    } else {
        alert("You are not allowed to visit this website");
    }
} else {
    alert(`Welcome ${userAnswerName}`);
}