//Опишите своими словами, как Вы понимаете, что такое Document Object Model (DOM)
//Он помогает связать работу JS с HTML документами


let array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let body = document.body;

let insertArray = (array, location) =>{
    location.insertAdjacentHTML('afterbegin', array.map(element => `<li>${element}</li>`).join(''));
}

insertArray(array, body);
