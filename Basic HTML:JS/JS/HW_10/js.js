let icon = document.querySelectorAll(".fas");

let show = () => {
    document.getElementById('first-input').setAttribute('type', 'text');
    document.getElementById('second-input').setAttribute('type', 'text');
    for (let item of icon) {
        item.className = 'far fa-eye-slash'
    }
}

let hide = () => {
    document.getElementById('first-input').setAttribute('type', 'password');
    document.getElementById('second-input').setAttribute('type', 'password');
    for (let item of icon) {
        item.className = 'far fa-eye'
    }
}

let pwShown = false;//сделал флажок
document.querySelector(".password-form").addEventListener("click", function (event) {
    if (event.target.tagName === 'I') {
        if (pwShown === false) {
            pwShown = true;
            show();
        } else {
            pwShown = false;
            hide();
        }
    }
}, false);

const showInValidResult = () => {
    let result = document.getElementById("error");
    if (result) {
        result.textContent = "Нужно ввести одинаковые значения";
    } else {
        let newElement = document.createElement("p");
        newElement.id = "error";
        newElement.classList.add("invalid-elem");
        newElement.textContent = "Нужно ввести одинаковые значения";
        newElement.style.cssText = 'color: red; font-weight: bold; font-size: 10px; margin-left: 2px';
        document.getElementById("second-input").after(newElement);
    }
};

document.querySelector(".btn").addEventListener("click", function () {
    let firstInput = document.getElementById("first-input").value;
    let secondInput = document.getElementById("second-input").value;
    if (firstInput === '' && secondInput === '') {
        alert('Ошибка! Введите значения в формы!');
    } else {
        if (firstInput === secondInput) {
            alert('You are welcome!');
            document.getElementById("error").remove();
        } else {
            showInValidResult();
        }
    }
})