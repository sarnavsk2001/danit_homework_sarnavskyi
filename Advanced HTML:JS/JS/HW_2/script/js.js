// Эта конструкция позволяет «ловить» ошибки и вместо падения скрипта делать что-то более осмысленное.

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const correctKey = ['author', 'name', 'price'];

let errorArray = [];


class ErrorArray extends Error {
    constructor() {
        super();
        this.message = `Error! Object without key: ${errorArray.toString()}`;
    }
}


function createElementUl() {
    let div = document.getElementById('root');
    let newUl = document.createElement('ul');
    newUl.className = 'list';
    div.append(newUl);
}


function displayCorrectArray(value) {
    value.forEach(element => {
        let arrayKeys = Object.keys(element);
        if (correctKey.toString() === arrayKeys.toString()) {
            let newLi = document.createElement('li');
            newLi.innerHTML = `<li>author: ${element.author}, name: ${element.name}, price: ${element.price}.</li>`;
            document.querySelector('.list').append(newLi);
        }
    })
}


function displayArray(value){
    value.forEach(element => {
        let arrayKeys = Object.keys(element);
        if (!(correctKey.toString() === arrayKeys.toString())) {
            errorArray = correctKey.filter(n => arrayKeys.indexOf(n) === -1);
            throw new ErrorArray();
        }
    })
}


try {
    createElementUl();
    displayArray(books)
} catch (err) {
    console.log(err.message);
} finally {
    displayCorrectArray(books);
}







