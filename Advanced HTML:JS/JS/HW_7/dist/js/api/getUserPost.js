
export default async function getUserPost () {
    let {data} = await axios.get("https://ajax.test-danit.com/api/json/posts")
    return data;
}