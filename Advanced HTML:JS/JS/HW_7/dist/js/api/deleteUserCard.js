
export default async function deleteUserCard(id) {
  const { status } = await axios.delete(`https://ajax.test-danit.com/api/json/posts/${id}`)
  return status;
}

