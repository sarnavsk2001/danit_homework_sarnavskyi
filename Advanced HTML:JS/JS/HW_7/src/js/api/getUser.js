export default async function getUser() {
  let { data } = await axios.get("https://ajax.test-danit.com/api/json/users");
  return data;
}
