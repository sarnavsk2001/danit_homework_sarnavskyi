import deleteUserCard from "../api/deleteUserCard.js";

export default class Card {
  constructor({ cardId, name, username, body, title }) {
    this.cardId = cardId;
    this.name = name;
    this.username = username;
    this.body = body;
    this.title = title;
  }

  render() {
    this.createCard();
    this.addtoPage();
    this.deleteCard();
  }

  createCard() {
    this.element = `
            <div class="card" someAttr="parentDiv" >
            <button id=${this.cardId} class="card__btn-close">X</button>
                <div class="card__part-name name-block">
                  <img class="name-block__image" src="./img/avatar.png" width="5%" height="auto">
                  <p class="name-block__name">${this.username} (${this.name})</p>
                </div>
                <p class="card__title">${this.title}</p>
                <p class="card__text">${this.body}</p>
            </div>`;
  }

  addtoPage() {
    document
      .querySelector(".section-content__cards")
      .insertAdjacentHTML("beforebegin", this.element);
  }

  deleteCard() {
    this.deleteCard = document.getElementById(`${this.cardId}`);
    this.deleteCard.addEventListener("click", async () => {
      console.log(this.cardId);
      const status = await deleteUserCard(this.cardId);
      if (status === 200) {
        document.getElementById(`${this.cardId}`).closest("div").style.display =
          "none";
      }
    });
  }
}
