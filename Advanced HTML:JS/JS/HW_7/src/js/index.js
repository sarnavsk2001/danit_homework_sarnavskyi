import getUser from "./api/getUser.js";
import getUserPost from "./api/getUserPost.js";
import Card from "./class/Card.js";

let users = await getUser();
let usersPost = await getUserPost();

let renamedusersPost = usersPost.map((obj) => {
  return {
    cardId: obj.id,
    title: obj.title,
    userId: obj.userId,
    body: obj.body,
  };
});

let newUserPost = renamedusersPost.map((post) =>
  Object.assign(
    post,
    users.find((user) => post.userId === user.id)
  )
);

newUserPost.forEach((post) => {
  const newCard = new Card(post);
  newCard.render();
});
