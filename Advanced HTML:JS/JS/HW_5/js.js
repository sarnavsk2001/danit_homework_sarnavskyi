const headers = {
    "Accept": "application/json",
    "Access-Control-Allow-Origin": "*",
}

let showIp = async () => {
    document.querySelector('.content-container').innerHTML = '';
    let ipRequest = await fetch('https://api.ipify.org/?format=json').then(res => res.json());
    let userInformation = await fetch(`http://ip-api.com/json/${ipRequest.ip}`, {headers}).then(res => res.json());
    const {timezone, country, region, city, regionName} = userInformation;
    let content = `<h2>Your information:</h2>
                       <p>Timezone: ${timezone}</p>
                       <p>Country: ${country}</p>
                       <p>Region: ${region}</p>
                       <p>City: ${city}</p>
                       <p>RegionName: ${regionName}</p>`;
    document.querySelector('.content-container').insertAdjacentHTML('beforeend', content);
}

document.querySelector('.btn-ip').addEventListener('click', showIp);