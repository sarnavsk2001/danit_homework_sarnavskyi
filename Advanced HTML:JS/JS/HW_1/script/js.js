//наследование это иерархия по древу, но свойства передаются детям и так дальше.


class Employee {

    constructor(name, age, salary) {
        this._name = name;
        this._salary = salary;
        this._age = age;
    }

    get age() {
        return this._age;
    }

    set age(value) {
        this._age = value;
    }

    get salary() {
        return this._salary;
    }

    set salary(value) {
        this._salary = value;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }
}


class Programmer extends Employee {

    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
        this._salary = salary;
    }

    get salary() {
        return this._salary * 3;
    }
}


const newProgrammer_1 = new Programmer("vlad", 20, 2000, 'JS');
const newProgrammer_2 = new Programmer("alina", 30, 3000, 'C#');

console.log(newProgrammer_1);
console.log(newProgrammer_1.salary);
console.log(newProgrammer_2);
console.log(newProgrammer_2.salary);
